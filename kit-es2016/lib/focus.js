/**
 * Created by admin on 24/05/16.
 */
import {Behavior} from 'aurelia-templating';

export class Focus {
    static metadata() {
        return Behavior
            .attachedBehavior('focus')
            .withProperty('value', 'valueChanged', 'focus');
    }

    static inject() { return [Element]; }
    constructor(element) {
        this.element = element;
    }

    valueChanged(value) {
        if (value) {
            this.element.focus();
        }
    }
}