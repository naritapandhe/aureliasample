import {Router} from 'aurelia-router';

export class App {
  static inject() { return [Router]; }
  message = 'Welcome!!';

  constructor(router) {
    this.router = router;
    this.router.configure(this.configureRoutes);
  }

  configureRoutes(cfg) {
    cfg.title = 'Sample App';
    cfg.map([
      {
        route:      ['','posts'],
        name:       'posts',
        moduleId:   './pages/posts/posts',
        nav:        true,
        title:      'Posts'
      },
      {
          route:    'addPost',
          name:     'addPost',
          moduleId: './pages/posts/addPost',
          nav:      true,
          title:    'Add Posts',
          
      },
      {
            route:      'comments',
            name:       'comments',
            moduleId:   './pages/comments/comments',
            nav:        true,
            title:      'Comments'
      },
      {
            route:      'search',
            name:       'search',
            moduleId:   './pages/search/search',
            nav:        true,
            title:      'Search'
      },
      {
            route:      'staticSearch',
            name:       'staticSearch',
            moduleId:   './pages/staticSearch/staticSearch',
            nav:        true,
            title:      'Static Search'
      }


    ]);
  }
}