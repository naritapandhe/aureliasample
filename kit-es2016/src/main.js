/**
 * Created by admin on 24/05/16.
 */
export function configure(aurelia) {
    aurelia.use
        .standardConfiguration();

    aurelia.start().then(a => a.setRoot());
}
