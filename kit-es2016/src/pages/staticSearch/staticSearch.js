/**
 * Created by admin on 01/06/16.
 */
/**
 * Created by admin on 27/05/16.
 */
import {bindable,customElement,ObserverLocator} from 'aurelia-framework';
import {inject} from 'aurelia-framework';
import {BookItem} from './bookItem';
import {BookParent} from '../parent/bookParent';
import _ from '../../../lib/underscore';
import 'fetch';

const ENTER_KEY = 13;
const ESC_KEY = 27;


@inject(BookParent,ObserverLocator)
export class Search {
    heading = 'Search an ISBN';
    _bookISBN = '1483340074';
    count = 0;

    get bookISBN() {
        return this._bookISBN;
    }

    set bookISBN(value) {
        this._bookISBN = value;
    }



    constructor(bookParent,observerLocator) {

        //Array of posts for the data
        this.books = [];

        this.observerLocator = observerLocator;
        this.bookParent = bookParent;
        this.expectedResults = 0;
        this.fetchedResults =  0;


    }

    //Lifecycle hook which fires just before the navigation finishes.
    //Useful to place all start-up logic in a consistent place.
    activate() {

        var self = this;
        if(self.count == 2){
            clearTimeout();
        }else {
            self.count++;
            // alert(this.bookISBN);
            //this.timer = setInterval(this.activate(),100000);
            return self.bookParent.activate('prices/' + self._bookISBN, 'get')
                .then(results => {
                    self.expectedResults = results.expected;
                    self.fetchedResults = results.fetched;
                    self.books = _.map(results.prices, item => {
                        const bookItem = new BookItem(item.VendorID, item.VendorName, item.Status, item.Price);


                        /**
                         * Making the model properties observables makes it capable of detecting changes,
                         * and when it does, it will update the view automatically.
                         */

                        // this.observeItem(bookItem);
                        return bookItem;
                    })
                }).then(setTimeout(this.activate(), 10000));
        }
    }

}

