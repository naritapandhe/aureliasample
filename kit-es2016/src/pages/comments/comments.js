/**
 * Created by admin on 26/05/16.
 */
/**
 * Created by admin on 24/05/16.
 */

import {bindable,customElement,ObserverLocator} from 'aurelia-framework';
import {inject} from 'aurelia-framework';
import {CommentItem} from './commentItem';
import {Parent} from '../parent/parent';
import {json} from 'aurelia-fetch-client';
import _ from '../../../lib/underscore';
import 'fetch';

const ENTER_KEY = 13;
const ESC_KEY = 27;


@inject(Parent,ObserverLocator)
export class Comments{

    heading = 'Comments';
    constructor(parent,observerLocator) {
        //Array of objects for the data
        this.comments = [];

        //Something for the comment to be editable
        this.newCommentBody = null;

        //Injecting dependencies
        this.parent=parent;
        this.observerLocator = observerLocator;


    }

    //Lifecycle hook which fires just before the navigation finishes.
    //Useful to place all start-up logic in a consistent place.
    activate() {

        //Invoke parent's respective method
        return this.parent.activate('comments')
               .then(comments => {
        this.comments =  _.map(comments, item => {
            const commentItem = new CommentItem(item.id,item.postId,item.name,item.email,item.body);

            /**
             * Making the model properties observables makes it capable of detecting changes,
             * and when it does, it will update the view automatically.
             */

             this.observeItem(commentItem);
             return commentItem;
            })
        });
    }


    //Method to observe changes of the linked properties
    observeItem(commentItem) {
        this.observerLocator
            .getObserver(commentItem, 'body')
            .subscribe((o, n) => this.onBodyChanged(commentItem));

        this.observerLocator
            .getObserver(commentItem, 'isCompleted')
            .subscribe(() => this.onIsCompletedChanged());

    }


    onBodyChanged(commentItem) {
        this.save(commentItem);
    }

    save(commentItem) {
        let data = {
            postId: commentItem.postId,
            name: commentItem.name,
            email: commentItem.email,
            body: commentItem.body
        };

        return this.parent.performAction('comments/' + commentItem.id,'put', data)
            .then( savedComment => {
                alert("Comment Updated!");
            }).catch(error=>{
                alert("Error updating comment!");
            });
    }

    delete(commentItem) {
        let data = {
            id: commentItem.id
        };

        return this.parent.performAction('comments/' + commentItem.id,'delete', data)
            .then( response => {
                this.comments = _(this.comments).without(commentItem);
                alert("Comment Deleted!");
            }).catch(error=>{
                console.log(error);
                alert("Error deleting comment!");
            });

    }


}

