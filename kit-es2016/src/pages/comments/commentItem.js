/**
 * Created by admin on 26/05/16.
 */
/**
 * Created by admin on 24/05/16.
 */
const ENTER_KEY = 13;
const ESC_KEY = 27;

export class CommentItem {
    constructor(id,postId,name,email,body) {
        this.id=id;
        this.postId=postId;
        this.name=name;
        this.email=email;
        this.body=body;
        this.isCompleted = false;
        this.isEditing = false;
        this.editBody = null;
    }

    labelDoubleClicked() {
        this.editBody = this.body;
        this.isEditing = true;
    }

    finishEditing() {
        this.body = this.editBody.trim();
        this.isEditing = false;
    }

    onKeyUp(ev) {
        if (ev.keyCode === ENTER_KEY) {
            return this.finishEditing();
        }
        if (ev.keyCode === ESC_KEY) {
            this.editBody = this.body;
            this.isEditing = false;
        }
    }
}