/**
 * Created by admin on 24/05/16.
 */
const ENTER_KEY = 13;
const ESC_KEY = 27;

export class PostItem {
    constructor(id,title,body) {
        this.id=id;
        this.body=body;
        this.isCompleted = false;
        this.isEditing = false;
        this.title = title.trim();
        this.editTitle = null;
    }

    labelDoubleClicked() {
        this.editTitle = this.title;
        this.isEditing = true;
    }

    finishEditing() {
        this.title = this.editTitle.trim();
        this.isEditing = false;
    }

    onKeyUp(ev) {
        if (ev.keyCode === ENTER_KEY) {
            return this.finishEditing();
        }
        if (ev.keyCode === ESC_KEY) {
            this.editTitle = this.title;
            this.isEditing = false;
        }
    }
}