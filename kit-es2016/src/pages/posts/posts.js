/**
 * Created by admin on 24/05/16.
 */

import {bindable,customElement,ObserverLocator} from 'aurelia-framework';
import {inject} from 'aurelia-framework';
import {PostItem} from './postItem';
import {Parent} from '../parent/parent';
import {HttpClient, json} from 'aurelia-fetch-client';
import _ from '../../../lib/underscore';
import 'fetch';

const ENTER_KEY = 13;
const ESC_KEY = 27;


@inject(Parent,ObserverLocator)
export class Posts {

    heading = 'Posts';

    constructor(parent,observerLocator) {

        //Array of posts for the data
        this.posts = [];

        //Something for the title to be editable
        this.newPostTitle = null;

        //Injecting dependencies
        this.observerLocator = observerLocator;
        this.parent = parent;


    }

    //Lifecycle hook which fires just before the navigation finishes.
    //Useful to place all start-up logic in a consistent place.
    activate() {

        //Invoke parent's respective method
        return this.parent.activate('posts')
            .then(posts => {
                this.posts =  _.map(posts, item => {
                    const postItem = new PostItem(item.id,item.title,item.body);


                    /**
                     * Making the model properties observables makes it capable of detecting changes,
                     * and when it does, it will update the view automatically.
                     */

                    this.observeItem(postItem);
                    return postItem;
                })
            });


    }

    //Method to observe changes of the linked properties
    observeItem(postItem) {
        this.observerLocator
            .getObserver(postItem, 'title')
            .subscribe((o, n) => this.onTitleChanged(postItem));

        this.observerLocator
            .getObserver(postItem, 'isCompleted')
            .subscribe(() => this.onIsCompletedChanged());

    }

    
    onTitleChanged(postItem) {
        this.save(postItem);
    }

    save(postItem) {
        let data = {
            title: postItem.title,
            body: postItem.body
        };

        return this.parent.performAction('posts/' + postItem.id,'put', data)
            .then( response => {
                    alert("Post Updated!");
                }).catch(error=>{
                    alert("Error updating post!");
                });
    }

    delete(postItem) {
        let data = {
            id: postItem.id
        };

        return this.parent.performAction('posts/' + postItem.id,'delete', data)
            .then( response => {
                this.posts = _(this.posts).without(postItem);
                alert("Post Deleted!");
            }).catch(error=>{
                alert("Error deleting post!");
            });

    }


}

