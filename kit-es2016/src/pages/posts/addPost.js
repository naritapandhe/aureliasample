/**
 * Created by admin on 24/05/16.
 */
import {inject} from 'aurelia-framework';
import {HttpClient, json} from 'aurelia-fetch-client';
import {Router} from 'aurelia-router';

import 'fetch';


/**
 * aurelia-fetch-client and aurelia-http-client
 * are used from HttpClient
 */
@inject(HttpClient,Router)
export class PostJson {
    heading = '';
    title = '';
    body = '';
    id = '';


    constructor(http,router){
        http.configure(config => {
            config
                .useStandardConfiguration()
                .withBaseUrl('http://localhost:3000/')
        });
            
        //Needed because all the GET/PUT/POST should hit these URLs
        this.http = http;

        //Needed inorder to navigate
        this.theRouter=router;
    }


    submit() {
        let data = {
            id:this.id,
            title: this.title,
            body: this.body
        };
        return this.http.fetch('posts', {
            method: 'post',
            body: json(data)
        }).then(response => response.json())
            .then(savedPost => {
                alert('Saved post!');
                this.theRouter.navigate("posts");
            }).catch(error => {
                alert('Error saving post!');

            });
    }
}