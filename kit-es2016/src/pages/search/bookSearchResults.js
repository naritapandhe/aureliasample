/**
 * Created by admin on 01/06/16.
 */
export class BookSearchResults {


   /* get books() {
        return this._books;
    }

    set books(value) {
        this._books = value;
    }
    get expectedResults() {
        return this._expectedResults;
    }

    set expectedResults(value) {
        this._expectedResults = value;
    }
    get fetchedResults() {
        return this._fetchedResults;
    }

    set fetchedResults(value) {
        this._fetchedResults = value;
    }*/


    activate(modelData) {
        this.expectedResults = modelData._expectedResults;
        this.fetchedResults = modelData._fetchedResults;
        this.books = modelData._books;

    }
}