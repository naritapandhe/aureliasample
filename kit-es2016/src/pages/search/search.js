/**
 * Created by admin on 27/05/16.
 */
import {bindable,customElement,ObserverLocator} from 'aurelia-framework';
import {inject} from 'aurelia-framework';
import {BookItem} from './bookItem';
import {BookParent} from '../parent/bookParent';
import {HttpClient, json} from 'aurelia-fetch-client';
import _ from '../../../lib/underscore';
import 'fetch';

const ENTER_KEY = 13;
const ESC_KEY = 27;

@inject(BookParent,ObserverLocator)
export class Search {

    heading = 'Search an ISBN';
    _count = 0;
    _fetchedResults = 0;
    _expectedResults = -1;
    _books = [];
    _observerLocator = '';
    _bookParent = null;
    _bookISBN = null;


    get expectedResults() {
        return this._expectedResults;
    }

    set expectedResults(value) {
        this._expectedResults = value;
    }
    get fetchedResults() {
        return this._fetchedResults;
    }

    set fetchedResults(value) {
        this._fetchedResults = value;
    }
    get count() {
        return this._count;
    }

    set count(value) {
        this._count = value;
    }

    get books() {
        return this._books;
    }

    set books(value) {
        this._books = value;
    }

    get observerLocator() {
        return this._observerLocator;
    }

    set observerLocator(value) {
        this._observerLocator = value;
    }

    get bookParent() {
        return this._bookParent;
    }

    set bookParent(value) {
        this._bookParent = value;
    }

    get bookISBN() {
        return this._bookISBN;
    }

    set bookISBN(value) {
        this._bookISBN = value;
    }




    constructor(bookParent,observerLocator) {

        //Array of posts for the data
        this._books = [];


        //Injecting dependencies
        this._observerLocator = observerLocator;
        this._bookParent = bookParent;

    }

    submit() {
       // alert('Searching prices for ISBN:'+ this.bookISBN);
        console.log('Searching prices for ISBN:'+ this.bookISBN);
        this.getPrices();
    }

    getPrices(){
       // alert("Getting the prices.....");
        console.log('Searching prices for ISBN:'+ this.bookISBN);
        var self  = this;
        if((self.count == 3)){
            clearTimeout();
        }else {
            self.count++;
            return self.bookParent.activate('/prices/' + self.bookISBN, 'get')
                .then(results => {
                    self.expectedResults = results.expected;
                    self.fetchedResults = results.fetched;
                    self.books = _.map(results.prices, item => {
                        const bookItem = new BookItem(item.VendorID, item.VendorName, item.Status, item.Price);

                        /**
                         * Making the model properties observables makes it capable of detecting changes,
                         * and when it does, it will update the view automatically.
                         */

                        return bookItem;
                    })
                }).then(setTimeout(self.getPrices(), 10000));
        }

    }
}

