/**
 * Created by admin on 26/05/16.
 */
import {bindable,customElement,ObserverLocator} from 'aurelia-framework';
import {inject} from 'aurelia-framework';
import {HttpClient, json} from 'aurelia-fetch-client';
import _ from '../../../lib/underscore';
import 'fetch';

const ENTER_KEY = 13;
const ESC_KEY = 27;

@inject(HttpClient)
export class Parent {

    constructor(http) {
         http.configure(config => {
             config
                 .useStandardConfiguration()
                 .withBaseUrl('http://localhost:3000/')
                 .withDefaults({
                     headers: {
                         'Access-Control-Allow-Origin': '*',
                     }
                 })
         });
         this.http = http;


    }

    activate(url){
        return this.http.fetch(url)
            .then(response => response.json());


    }

    performAction(url,method,data) {
        return this.http.fetch(url, {
            method: method,
            body: json(data)
        }).then(response => response.json());
    }


}