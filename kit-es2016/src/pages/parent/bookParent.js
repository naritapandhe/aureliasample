/**
 * Created by admin on 27/05/16.
 */
/**
 * Created by admin on 26/05/16.
 */
import {bindable,customElement,ObserverLocator} from 'aurelia-framework';
import {inject} from 'aurelia-framework';
import {HttpClient, json} from 'aurelia-fetch-client';
import _ from '../../../lib/underscore';
import 'fetch';

const ENTER_KEY = 13;
const ESC_KEY = 27;

@inject(HttpClient)
export class BookParent {

    constructor(http) {
       http.configure(config => {
            config
                .useStandardConfiguration()
                .withBaseUrl('http://ec2-52-90-64-158.compute-1.amazonaws.com/v1/')
                .withDefaults({
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                        'Access-Control-Allow-Methods': 'GET, PUT, POST, OPTIONS',
                        'Access-Control-Allow-Headers': 'Content-Type',
                        'Access-Control-Allow-Credentials':true
                    }
                })
        });
        this.http = http;


    }

    activate(url){
        return this.http.fetch(url)
            .then(response => response.json());


    }

    performAction(url,method,data) {

        if(data==null){
            return this.http.fetch(url, {
                method: method,
            }).then(response => response.json());
        }else {
            return this.http.fetch(url, {
                method: method,
                body: json(data)
            }).then(response => response.json());

        }
    }


}